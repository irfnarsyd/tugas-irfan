const express = require('express')
const app = express()
const port = 4000
const user = require("./router/user")
const blogpost = require("./router/blogpost")

app.use(express.json())

app.use("/user", user)
app.use("/", blogpost)

app.listen(port, () => console.log(`Server live on port:${port}`))