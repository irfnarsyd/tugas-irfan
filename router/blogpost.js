const express = require('express')
const router = express.Router()
const authenticated = require("../middleware/authenticated")
const validationBlog = require("../middleware/validationBlog")

let blogPost = require("../db/blogPost.json")

router.post("/blog", authenticated,validationBlog, (req,res) => {
    
    const {title , content, likes} = req.body
    const currentBlogpostId = blogPost.length && blogPost[blogPost.length-1].id || 0
    let blogpostId =  currentBlogpostId + 1
    const checkDuplicate = blogPost.find(item => item.title === title)

    if(checkDuplicate !== undefined){
        res.send("Title is already exist!")
        return
    }

    blogPost.push({
        id: blogpostId,
        user_id: req.userData.id,
        title,
        content,
        likes:0
    })

    return res.send({
        id: blogpostId,
        message: "blog has been added!"
    })

})

router.get("/blog", (req,res) =>{
    res.status(400).send(blogPost)
})

router.put("/blog/:id", (req,res) =>{
    let post = blogPost.find(i => i.id == req.params.id)
    const params = {likes: post.likes + 1}
    post = {...post, ...params}
    blogPost = blogPost.map(i => i.id == post.id ? post:i)

    res.status(200).json({post, message: "likes is given to the post!!"})
})

module.exports = router