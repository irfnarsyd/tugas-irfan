const express = require('express')
const router = express.Router()
const validation = require('../middleware/validationUser')
const jwt = require("jsonwebtoken")

let users = require("../db/users.json")

router.post('/register', validation, (req, res) => {
    const {name, email,password} = req.body
    const getID = (users.length && users[users.length - 1].id || 0)
    let id = getID + 1

    const checkDuplicate = users.find(item => item.email === email)

    if(checkDuplicate === undefined){
        users.push ({id,name,email,password})
        return res.status(200).json({users, message: "User Created"})
}
    return res.status(400).send("User is already exist")
})

router.post('/login',(req, res) => {
    const {email, password} = req.body;
    const isFoundUser = users.find((item) => item.email === email)
    if(isFoundUser) {
        const isValidPassword = isFoundUser.password === password
        if(isValidPassword) {
            const jwtPayload = jwt.sign({
                name: isFoundUser.name, 
                id: isFoundUser.id
            }, 'token')
            return res.json({token: jwtPayload, message: "Login Success"})
        }
    }
    return res.status(400).send("Login failed")
})



module.exports = router